import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    /**
     * El método main jamás debe lanzar una excepción 
     */
    public static void main(String[] args) throws IOException {

        //aquí definimos los parametros y le asignamos el comando que hay en 'args' que es 'ls -la'
        List<String> argList = new ArrayList<>(Arrays.asList());
        ProcessBuilder hijo = new ProcessBuilder(args);
        Process padre = hijo.start();

        //aquí le ponemos el tiempo que el padre está esperando y si falla lo notifica
        try {
            if(!padre.waitFor(2, TimeUnit.SECONDS)){
                throw new InterruptedException(); 
            }
            //aquí ponemos el mensaje que da si el tiempo se agota y el System.exit finaliza el programa
        } catch (InterruptedException e) {
            System.out.println("Tiempo de espera agotado");
            System.exit(-1);
        }

        /**
         * Se ha de comprobar si el proceso ha finalizado correctamente o no.
         * Esto es mediante hijo.exitStatus o hijo.waitFor sin argumentos. 
         * 
         * Porque el ejercicio pide obtener el error del proceso. 
         * Por ejemplo, hacer un ls de un proceso que no existe.
         */

        InputStream is = padre.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        //aquí está la ruta de donde guardamos el fichero
        BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
        //dos variables strings creadas para poder guardar la información en un fichero
        String line;
        String texto = "";

            while((line = br.readLine()) != null) {
                try {
                    //aquí muestra el contenido del line (que es el 'ls -la')
                    System.out.println(line);
                    //
                    /**
                     * Al utilizar bufferedReader no hace falta poner el salto de líonea.
                     * en general poner todo el string en una variable es una mala praxis.
                     * 
                     * 
                     * Con newline indicamos que se trata de una nueva línea y la mv ya
                     * pone el caracter correcto en función de la plataforma
                     */
                    bw.write(line);
                    bw.newLine();
                } catch (Exception e){
                    //aquí ponemos el mensaje de error si el hijo falla
                System.out.println("El hijo ha fallado");
                }
            }
            //escribe el contenido de texto en el fichero
            bw.close();

            /**
             * Faltan bloques try por todas partes.
             */
    }
}
